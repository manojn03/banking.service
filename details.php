<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Banking Transactions</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">


  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">


  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">


  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">

 
  <link href="assets/css/style.css" rel="stylesheet">

  


</head>

<body>


  <section id="contact" class="contact ">
      <div class="container">
        <div class="col-lg-12  align-items-stretch" data-aos="fade-up">
         <div class="info-box " data-aos="fade-up" data-aos-delay="300">
            <div class="section-title" >
              <h2>Account Details</h2>
            </div>
              <?php
                error_reporting(0);
                include_once("include/dbconnect.php");
                  if($_SERVER['REQUEST_METHOD']=='POST'){
                    session_start();
                    $account_num=$_POST['account_num'];
                  }
                    $_SESSION['account_num']=$account_num;


                  $sql = "SELECT  * FROM customer where account_num='".$account_num."'";

                   $result = $conn->query($sql);

                  if ($result->num_rows > 0) {
                    
                    while($row = $result->fetch_assoc()) {
                      echo "Account Number : " . $row["account_num"]. "<br>";
                      echo "Account Name : " . $row["account_name"]. "<br>";
                      echo "Account Type : " . $row["bank_type"]. "<br>";
                      echo "City : " . $row["city"]. "<br>";
                      echo "Branch Name : " . $row["branch_name"]. "<br>";
                      echo "Mobile Number : " . $row["mobile"]. "<br>";
                      echo "Balance Amount : Rs " . $row["balance"]. "<br>";

                      $_SESSION['account_num']=$row["account_num"];
                      $_SESSION['account_name']=$row["account_name"];
                      $_SESSION['bank_type']=$row["bank_type"];
                      $_SESSION['city']=$row["city"];
                      $_SESSION['branch_name']=$row["branch_name"];
                      $_SESSION['mobile']=$row["mobile"];
                      $_SESSION['balance']=$row["balance"];
                      
                    }
                  } else {
                    echo "Enter Details";
                  }
                  $conn->close();
                   
            ?>
          </div>
        </div>
      </div>
    </section>
          


          <div class="col-lg-12" data-aos="fade-up" data-aos-delay="300">
            <form action="transfer.php" method="post" role="form" class="php-email-form">
              <div class="form-row">
                <div class="section-title" >
                  <h2>Transfer Amount</h2>
                </div>
                <div class="col-lg-10 form-group">
                  <h5>Account Details</h5>
                  <select type="text" name="to_acc_num" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" required />
                   <option value="" disabled selected >Select Account Number</option>
                   <option value="SBI00011101">SBI00011101</option>
                   <option value="SBI00021102">SBI00021102</option>
                   <option value="SBI00031103">SBI00031103</option>
                   <option value="SBI00041101">SBI00041101</option>
                   <option value="SBI00051102">SBI00051102</option>
                   <option value="SBI00051203">SBI00051203</option>
                   <option value="SBI00061101">SBI00061101</option>
                   <option value="SBI00071103">SBI00071103</option>
                   <option value="SBI00081103">SBI00081103</option>
                   <option value="SBI00091108">SBI00091108</option>
                  
                 </select>

                  <div class="validate"></div>
                </div>
                <div class="col-lg-6 form-group">
                  <h5>Transfer Amount in Rs:</h5>
                  <input type="number" class="form-control" name="t_amount" id="email" placeholder="Amount in Rs" required="" /> <br>
                  <div class="validate"></div>
                  <div class="text-center"><button type="submit"> Tranfer Amount </button></div>
                  
                </div>
              
              
              
              <div class="mb-3">
                <div class="loading">Loading</div>
                <div class="error-message"></div>
                <div class="sent-message">Your message has been sent. Thank you!</div>
              </div>
            </form>
          </div>

        </div>

      </div>
    </section>
<br>


  

  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/jquery-sticky/jquery.sticky.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

  <script src="assets/js/main.js"></script>

</body>

</html>