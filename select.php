<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1.0" name="viewport">

	<title>Banking Transactions</title>
	<meta content="" name="descriptison">
	<meta content="" name="keywords">


	<link href="assets/img/favicon.png" rel="icon">
	<link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">


	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">


	<link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
	<link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
	<link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
	<link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
	<link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
	<link href="assets/vendor/aos/aos.css" rel="stylesheet">

 
	<link href="assets/css/style.css" rel="stylesheet">

	


</head>

<body>


	<header id="header">
		<div class="container">

			<div class="logo float-left">
				<h1 class="text-light float-right"><a href="index.html"><span><b>Banking Service</b></span></a></h1>
			</div>

			<nav class="nav-menu float-right d-none d-lg-block">
				<ul>
					<li><a href="index.html">Home</a></li>
					<li class="active"><a href="customers.php"><b>Customers</b></a></li>
					<li><a href="transactions.php">Transactions</a></li>
				</ul>
			</nav>

		</div>
	</header>


<section id="contact" class="contact">
			<div class="container">

				<div class="section-title">
					<h2>Select Customer</h2>
				</div>

				

					<div class="col-lg-12" data-aos="fade-up" data-aos-delay="300">
						<form action="details.php" method="post" role="form" class="php-email-form">
							<div class="form-row">
								<div class="col-lg-10 form-group">
									<select type="text" name="account_num" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" required />
									 <option value="" disabled selected >Select Account Number</option>
									 <option value="SBI00011101">SBI00011101</option>
									 <option value="SBI00021102">SBI00021102</option>
									 <option value="SBI00031103">SBI00031103</option>
									 <option value="SBI00041101">SBI00041101</option>
									 <option value="SBI00051102">SBI00051102</option>
									 <option value="SBI00051203">SBI00051203</option>
									 <option value="SBI00061101">SBI00061101</option>
									 <option value="SBI00071103">SBI00071103</option>
									 <option value="SBI00081103">SBI00081103</option>
									 <option value="SBI00091108">SBI00091108</option>
									
								 </select>

									<div class="validate"></div>
								</div>
								
							
							
							<div class="text-center"><button type="submit"> More Detais </button></div>
							<div class="mb-3">
								<div class="loading">Loading</div>
								<div class="error-message"></div>
								<div class="sent-message">Your message has been sent. Thank you!</div>
							</div>
						</form>
					</div>

				</div>

			</div>
		</section><!-- End Contact Us Section -->
<br>


	<footer id="footer">
		<div class="footer-top">
			<div class="container">
				<div class="row">

					<div class="col-lg-6 col-md-6 footer-info">
						<h3>Banking Service</h3>
					</div>

					<div class="col-lg-6 col-md-6 footer-links">
						<h4>Useful Links</h4>
						<ul>
							<li><i class="bx bx-chevron-right"></i> <a href="index.html">Home</a></li>
							<li><i class="bx bx-chevron-right"></i> <a href="customers.php">Customers</a></li>
					    <li><i class="bx bx-chevron-right"></i> <a href="transactions.php">Transactions</a></li>
						</ul>
					</div>

				</div>
			</div>
		</div>

		
	</footer>

	<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

	<script src="assets/vendor/jquery/jquery.min.js"></script>
	<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
	<script src="assets/vendor/php-email-form/validate.js"></script>
	<script src="assets/vendor/jquery-sticky/jquery.sticky.js"></script>
	<script src="assets/vendor/venobox/venobox.min.js"></script>
	<script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
	<script src="assets/vendor/counterup/counterup.min.js"></script>
	<script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
	<script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
	<script src="assets/vendor/aos/aos.js"></script>

	<script src="assets/js/main.js"></script>

</body>

</html>