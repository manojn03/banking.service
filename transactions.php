<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Banking Transactions</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">


  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">


  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">


  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">

 
  <link href="assets/css/style.css" rel="stylesheet">

  <style>

.topnav a.stayn{
  color: #FF0038;
}


h1 {
  color: white;
}

button {
  color: white;
  background-color: #428bca;
  border: 0;
  width: 120px;
  height: 40px;
  font-weight: bold;
  margin-left:auto; 
  margin-right:auto;
}
    
table {
  border-collapse: collapse;
  margin-left:auto; 
  margin-right:auto;
  width: 80%;
  }

th, td {
  text-align: center;
  border: 1px solid white;
  padding: 12px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
  background-color: #ff9800;
  color: white;
}
</style>


</head>

<body>


  <header id="header">
    <div class="container">

      <div class="logo float-left">
        <h1 class="text-light float-right"><a href="index.html"><span><b>Banking Service</b></span></a></h1>
      </div>

      <nav class="nav-menu float-right d-none d-lg-block">
        <ul>
          <li><a href="index.html">Home</a></li>
          <li><a href="customers.php">Customers</a></li>
          <li class="active"><a href="transactions.php"><b>Transactions</b></a></li>
        </ul>
      </nav>

    </div>
  </header>

  <br>
    <div class="col-lg-12 col-md-12 ">
    <?php

      error_reporting(0);
      session_start();
      include_once("include/dbconnect.php");

      echo '<br>';
       $sql = "SELECT  * FROM transactions order by id";

       $result = $conn->query($sql);

      if ($result->num_rows >0) {

          echo "<table> <tr> <th>Transactions ID</th> <th>Transactions Date & Time</th> <th>Sender Account Number</th> <th>Sender Name</th> <th>Available Balance in Rs</th> <th>Receiver Account Number</th> <th>Receiver Name</th> <th>Transfered Amount in Rs</th>    <th>Sender Current Balance in Rs</th> </tr>";
          // output data of each row
          while($row = $result->fetch_assoc()) {
              echo "<tr> <td>" . $row["id"]. "</td> <td>" . $row["created_at"]. "</td> <td>" . $row["account_num"]. "</td> <td>" . $row["account_name"]. "</td> <td>&#8377; " . $row["balance"]. "</td> <td>" . $row["to_acc_num"]. "</td>  <td>" . $row["to_acc_name"]. "</td>  <td>- &#8377;" . $row["t_amount"]. "</td>  <td>&#8377; " . $row["c_balance"]. "</td></tr>";                   
          }
          
          echo "</table>";
      } else {
          echo "<h1>No Customers found</h1>";
      }


      $conn->close();
      ?>
      <div style="margin-left: ">
        
      </div>
      
      </div>

<br>

  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-6 col-md-6 footer-info">
            <h3>Banking Service</h3>
          </div>

          <div class="col-lg-6 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="index.html">Home</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="customers.php">Customers</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="transactions.php">Transactions</a></li>
            </ul>
          </div>
 
        </div>
      </div>
    </div>

    
  </footer>

  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/jquery-sticky/jquery.sticky.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

  <script src="assets/js/main.js"></script>

</body>

</html>